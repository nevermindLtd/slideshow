// Copyright (C) 2017 Ange Abou - Polytech'Nice
//
// Ce programme est un logiciel libre; Il peut �tre redistribu� et/ou
// modifi� sous les termes de la "GNU General Public License" (Licence
// G�n�rale Publique GNU) comme publi�s par la Free Software Foundation;
// soit la version 2, soit (au choix) toute version ult�rieure.
//
// Ce programme a une vocation �ducative, et est distribu� sans aucune
// garantie de fonctionnement; sans la garantie implicite de pouvoir 
// le commercialiser ou m�me de r�pondre � un besoin particulier.
// Se r�f�rer � la "GNU General Public License" pour plus de d�tails.
//
// Date de cr�ation: Mai 2017

#include "SlideShow.hpp"
#include <iostream>
// Pour g�n�rer des valeurs enti�res al�atoires uniform�ment r�parties
#include <random>

using namespace std;


#define CHECK_IF_INITIALISED() \
    if (!_isInitialised) { \
        cerr << "Attention: appeler d'abord init sur objet SlideShow." << endl;\
        return 0;\
    }

/* */
SlideShow::SlideShow(const char* iFilePath) 
    : _filepath(iFilePath)
    , _filestream(iFilePath)
    , _nbFilelines(0) 
    , _isInitialised(false) {
    
}

/* */
SlideShow::SlideShow(const std::string iFilePath) 
    : SlideShow(iFilePath.c_str()) {
    // Ce constructeur avec param�tre 'const string' r�utilise le constructeur
    // pr�c�dent avec param�tre 'const char*'
}
 
/* */
bool SlideShow::init() {
    ifstream _filestream(_filepath, ifstream::in);
    if (_filestream.good()) {
        this->_nbFilelines=0;
        string line;
        while (getline(_filestream, line)) {
             this->_nbFilelines++;
        }
        this->_isInitialised=true;
        /*
        * A la compilation et l'execution des tests on constate 2 "test case" et 4 assertions qui 
        * ne sont pas valides. On remarcque �galement les commentaires "Attention: appeler d'abord 
        * init sur objet SlideShow." plusieurs fois. De plus un r�pertoir "Debug" a �t� cr�� avec 
        * les ficher *.o
        */
    } else {
        cerr << "Erreur: impossible d'ouvrir le fichier." << endl;
        this->_isInitialised=false;
        return false;
    }
    
    return true;
}

const std::uint64_t SlideShow::getNumberOfSlides() {
    CHECK_IF_INITIALISED();
    
    return _nbFilelines;
}

/*
 http://fr.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
 Pour produire des valeurs enti�res al�atoires uniform�ment r�parties
 */
const std::uint64_t SlideShow::getRandomNumber() {
    CHECK_IF_INITIALISED();
    
    static random_device rd;
    static mt19937 gen; // g�n�rateur
    static uniform_int_distribution<> dis(1, this->_nbFilelines);
    /*
    * - Pourquoi on utilise le mot cl� 'static' ici ? (Contrairement au code en ligne)
    *   Car ces valeurs n'ont pas vocation � changer dans la suite du code.
    */
    
    return dis(gen);
}

const std::uint8_t SlideShow::displayRandomSlide(std::ostream* out) {
    std::uint8_t rc = 0; // code de retour si OK
    if (!_isInitialised) {
        cerr << "Attention: appeler d'abord init sur objet SlideShow." << endl;
        rc = 1; // code d'erreur si non initialis�
    }

    std::uint64_t random=this->getRandomNumber();
    string slide;
    for(std::uint64_t i=0; i<random;i++){
        getline(this->_filestream, slide);
    }
    *out << slide << endl;
    
    return rc;
}
